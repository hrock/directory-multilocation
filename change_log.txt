v1.4.4
The neighbourhood system is improved - CHANGED
Site in subfolder and url to main domain can break location links - FIXED
Made few changes for W3C validation - CHANGED
WooCommerce shop page can sometimes show no products - FIXED
New update licence system implemented - CHANGED
Function geodir_get_limited_country_dl() fails to select translated country names - FIXED
Location filter added in back-end post type listing pages - ADDED
Options added to exclude location pages from Yoast SEO XML sitemap - CHANGED
Image and tagline option added to SEO page - ADDED
[gd_current_location_name] shortcode should display "Near Me" when near me location is selected - CHANGED
Confirm message text displayed on delete location updated - CHANGED
Option added to import/export location data via GD import/export - ADDED
Sessions managed by GeoDirectory Session class - CHANGED
Validating and sanitizing changes - FIXED

v1.4.3
Settings Region -> Enable Selected Regions the countries listings page shows 404 not found - FIXED
Select Cityx renamed to Select City - FIXED
Location/me sometimes returns no map results - FIXED
location/me not setting as GD page - FIXED
Blog paged link can contain location info - FIXED
Added rewrite rules for details page comments paging - ADDED
If GD is not set as homepage the location is not reset when visiting homepage - FIXED
Now able to generate sitemap links for location pages via Yoast SEO plugin - ADDED

v1.4.2
case conversion functions replaced with custom function to support unicode languages - CHANGED
Homepage page_id query var not set and can cause problems with Yoast SEO - FIXED
If default country set, it can set a session on non location urls - FIXED
Manage seo settings customized by using pagination and search filter - CHANGED

v1.4.1
Locations with apostrophes can cause problems with locations when adding listings - FIXED
Some error pages not using geodir_login_url() as redirect - FIXED
City/region values goes blank in listing form when clicking go back and edit on the preview page - FIXED
Settings added to hide country/region part from urls – ADDED
Fixed term data count for multiple location names with same name - FIXED
Fix dbDelta sql for upgrades - FIXED
Limiting location on add listing page not throwing errors sometimes - FIXED
Trailing slash add for home_url() and site_url() urls using trailingslashit() - CHANGED

v1.4.0
Added filter, geodir_location_description to be able to filter the location description text - ADDED
Show 404 page not found on location page if location not exists - CHANGED
Limiting countries and having them translated can cause duplicate location entries - FIXED
Changing location switcher tab loads all locations and then continues to ajax load them again - FIXED
Fixed conflict canonical url on location pages with Yoast WordPress SEO plugin - FIXED
Location is now automatically unset if user searches a "near" location that is not a autosugestion of actual location - CHANGED

v1.3.9
Some servers can interperate the merge fileds ajax call as json which means the submit button is never shown - FIXED
GD auto update script improved efficiency and security(https) - CHANGED
If page set as blog page WPML page_id being set will break page for translation - FIXED
Geo-locate user now works if default location is set to load on first load - CHANGED
Changed textdomain from defined constant to a string - CHANGED

v1.3.8
Filters added in reviews count query - CHANGED
All widgets changed from PHP4 style constructors to PHP5 __construct, for WordPress 4.3 - CHANGED
`Select Neighbourhood` missing textdomain and not translatable - FIXED
In some circumstances the location switcher can have one location not selectable - FIXED
Added filter to filter the default location tab when no location selected - ADDED
Changes made for WPML compatibility - CHANGED

v1.3.7
Function geodir_get_current_location() made much more efficient when being called multiple times on one page - CHANGED
Set location not working correctly when only city added to place urls - FIXED
Some docblocks added - ADDED

v1.3.6
dbDelta function used for db tables creation - CHANGED

v1.3.5
Added option to stop add listing map pin move from changing the address - ADDED
In backend searching for location not redirecting properly - FIXED
Pagination not working in backend manage location - FIXED
Function codeAddress changed to geodir_codeAddress for compatibility - CHANGED

v1.3.4
fix for geodir_single_next_previous_fix() function - FIXED
Pagination and filter option added in admin manage location page - ADDED
Checked for XSS vulnerabilities as per latest WP security update, found none but updated the code to new standards - SECURITY
New filter added for count location terms - ADDED

v1.3.3
term description sometimes not showing - FIXED
Country/Region/City add listing page titles not translatable form .po file - FIXED

v1.3.2
Location specific category counts can be wrong/not updated correctly - FIXED

v1.3.1
Popular category link not working with ajax - FIXED
Prev/Next function checking for post_type when not needed - CHANGED

v1.3.0
Location/me page can loop when GD Booster is installed - FIXED
Near me button widget title can add slash in front of apostrophe - FIXED
After clicking near me button value on search page displays "1" when no advanced search - FIXED
Prev/Next links can show attachments instead of posts - FIXED

v1.2.9
Location switcher can show wrong locations when drilling down if similar country names present - FIXED
Add listing page address labels get reset to default on upgrade - FIXED
Near me button widget not working if advanced search is not installed - FIXED

v1.2.8
Prefixed all shortcodes with gd_ - CHANGED

v1.2.6
Listing appear in wrong location if region and city have the same name - FIXED
added change to allow address autocomplete work with add-listing shortcode - FIXED
Added more shortcodes and fixed the ones that were there - ADDED

v1.2.5
Location selector will now do split word search (you can search 'kingdom' for 'united kingdom' now)- CHANGED
added more class filters for location switcher for menu - ADDED
Show default location results on home page now working - FIXED

v1.2.4
changed $ to jQuery in some scripts for compatability - CHANGED

v1.2.3
added filter to add class to location switcher menu item (required for X theme) - ADDED

v1.2.2
prev/next links on details page can show link to original post - FIXED

v1.2.1
extended mobile location switcher alternative to iPad - FIXED

v1.2.0
Removed the need for shortcode option of autoredirect on location shortcode - CHANGED
Option to List all Countries, Regions, Cities in location swtcher now working - FIXED
Check added to see if core GeoDirectory is active before loading the rest of the plugin - ADDED
Location switcher not working on avada or themes where mobile menu is auto generated - FIXED
Url redirect problem for crawler if location url has not trailing slash - FIXED
Ajax search not working in location tab switcher in mobile device - FIXED
WordPres multisite compatability - ADDED
Country translate page, added instructions - ADDED
added option to show all location in add listing dropdown - ADDED
added option to stop "set address on map" from changing address fields on add listing page - ADDED

V1.1.3
Unique category description for each location seems to be displayed depending on SESSION which is not good as crawlers will not see the correct description - FIXED
Added ability to correct region data from google api - ADDED
Slovakian regions array added - ADDED
Location url prefix meta title "Location" not translated - FIXED
Little fix when displaying listings by neighbourhood - FIXED
Location switcher shortcode doesn't redirect if placed in a sidebar - FIXED
Added translatation for region & city in breadcrumb - ADDED
